const button = document.getElementById('generate');
const textIn = document.getElementById('in');
const textOu = document.getElementById('out');

button.onclick = event => {
    const value = textIn.value;
    const array = value.split('\n');
    const code = [];

    array.forEach(string => {
        if (!string) return;

        code.push(`<code>${string}</code>`);
    });

    textOu.value = code.join('\n');
}